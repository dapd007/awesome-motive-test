<?php


class AwesomeNotices
{
    /**
     * Key for temporary notice storage
     *
     * @var string $OPTION_KEY Key for temporary notice storage
     */
    private static $OPTION_KEY = 'awesome_flash_notices';

    /**
     * Temporarily add flash notice to storage
     *
     * @param string $notice
     * @param string $type
     * @param bool $dismissible
     */
    public static function add_flash_notice( $notice = "", $type = "success", $dismissible = true ) {
        // Here we return the notices saved on our option, if there are not notices, then an empty array is returned
        $notices = get_option( self::$OPTION_KEY, array() );

        $dismissible_text = ( $dismissible ) ? "is-dismissible" : "";

        // We add our new notice.
        array_push( $notices, array(
            "notice" => $notice,
            "type" => $type,
            "dismissible" => $dismissible_text
        ) );

        // Then we update the option with our notices array
        update_option( self::$OPTION_KEY, $notices );
    }

    /**
     * Display flash messages and delete them afterwards.
     */
    public static function display_flash_notices() {
        $notices = get_option( self::$OPTION_KEY, array() );

        // Iterate through our notices to be displayed and print them.
        foreach ( $notices as $notice ) {
            printf('<div class="notice notice-%1$s %2$s"><p>%3$s</p></div>',
                $notice['type'],
                $notice['dismissible'],
                $notice['notice']
            );
        }

        // Now we reset our options to prevent notices being displayed forever.
        if( ! empty( $notices ) ) {
            delete_option( self::$OPTION_KEY );
        }
    }
}