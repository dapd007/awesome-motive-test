<?php


class AwesomeRequest
{
    /**
     * Curl
     *
     * @var
     */
    private $curl;

    /**
     * AwesomeRequest constructor.
     * @param $url string Url
     */
    public function __construct($url) {
        $this->prepare($url);
    }

    /**
     * Prepare request, set options
     *
     * @param $url string Url
     */
    private function prepare($url) {
        $this->curl = curl_init();

        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->curl, CURLOPT_FAILONERROR, true);
    }

    /**
     * Execute request
     *
     * @return bool|string
     */
    public function execute() {
        return curl_exec($this->curl);
    }
}