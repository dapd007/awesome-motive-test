<?php


class AwesomeMotive
{
    /**
     * Shortcodes handlers
     *
     * @var AwesomeShortcodes $shortcodes Shortcodes handlers
     */
    protected $shortcodes;

    /**
     * Ajax handlers
     *
     * @var AwesomeAjax $ajax Ajax handlers
     */
    protected $ajax;

    /**
     * Public-facing features and hooks
     *
     * @var AwesomeMotivePublic $public Public-facing features and hooks
     */
    protected $public;

    /**
     * Admin-facing features and hooks
     *
     * @var AwesomeMotivePrivate $private Admin-facing features and hooks
     */
    protected $private;

    /**
     * AwesomeMotive constructor.
     *
     * Loads dependencies and registers hooks
     *
     */
    public function __construct()
    {
        $this->load_dependencies();
        $this->define_admin_hooks();
        $this->define_public_hooks();
    }

    /**
     * Load dependencies and set handlers variables
     */
    private function load_dependencies() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-awesome-request.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-awesome-cache.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-awesome-motive-core.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-awesome-shortcodes.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-awesome-ajax.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-awesome-notices.php';

        if ( defined( 'WP_CLI' ) && WP_CLI ) {
            require_once plugin_dir_path( dirname( __FILE__ ) ) . 'cli/class-awesome-motive-cli.php';
            AwesomeMotiveCLI::init();
        }

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-awesome-motive-public.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-awesome-motive-private.php';

        $this->shortcodes = new AwesomeShortcodes();
        $this->ajax = new AwesomeAjax();

        $this->public = new AwesomeMotivePublic();
        $this->private = new AwesomeMotivePrivate();
    }

    /**
     * Register shortcodes on AwesomeShortcodes
     */
    public function register_shortcodes() {
        add_shortcode('awesome-table', [$this->shortcodes, 'awesome_table']);
    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     */
    private function define_admin_hooks() {
        //  Admin menu
        add_action('admin_menu', [$this->private, 'create_admin_menu']);

        //  Clear Cache post action
        add_action('admin_post_clear_awesome_cache', [$this->private, 'action_clear_awesome_cache']);

        //  Admin notices
        add_action('admin_notices', 'AwesomeNotices::display_flash_notices');
    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     */
    private function define_public_hooks() {
        //  Styles
        add_action('wp_enqueue_scripts', [$this->public, 'enqueue_styles']);

        //  Scripts
        add_action('wp_enqueue_scripts', [$this->public, 'enqueue_scripts']);

        //  Shortcodes
        add_action('init', [$this, 'register_shortcodes']);

        //  Ajax
        add_action( 'wp_ajax_get_table_data', [$this->ajax, 'get_table_data'] );
        add_action( 'wp_ajax_nopriv_get_table_data', [$this->ajax, 'get_table_data'] );
    }
}