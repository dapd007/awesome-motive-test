<?php

/**
 * Class AwesomeCache
 *
 * Simple WP Transient wrapper to manage plugin cache
 *
 */
class AwesomeCache
{
    /**
     * Cache Key
     *
     * @var string $CACHE_DATA_KEY Cache Key
     */
    private static $CACHE_DATA_KEY = 'awesome_table';

    /**
     * Cache Expiry
     *
     * Cache expiration in seconds.
     *
     * @var int $CACHE_EXPIRY Cache Expiry
     */
    private static $CACHE_EXPIRY = 3600; // 1 hour

    /**
     * Get cached data
     *
     * @return mixed Cached data
     */
    public static function get_data() {
        return get_transient(self::$CACHE_DATA_KEY);
    }

    /**
     * Update cached data
     *
     * @param $data mixed Data to cache
     */
    public static function save_data($data) {
        set_transient(self::$CACHE_DATA_KEY, $data, self::$CACHE_EXPIRY);
    }

    /**
     * Clear cached data
     *
     * @return bool
     */
    public static function clear_data() {
        return delete_transient(self::$CACHE_DATA_KEY);
    }
}