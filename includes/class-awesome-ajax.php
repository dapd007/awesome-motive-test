<?php

/**
 * Class AwesomeAjax
 *
 * Ajax handlers
 */
class AwesomeAjax
{
    /**
     * Get table data from Core
     */
    public function get_table_data() {
        echo json_encode(AwesomeMotiveCore::get_data());
        wp_die();
    }
}