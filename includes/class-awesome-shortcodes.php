<?php

/**
 * Class AwesomeShortcodes
 */
class AwesomeShortcodes
{
    /**
     * Wrap content with simple Vue App
     *
     * @param $content
     * @return string
     */
    private function wrap_with_app($content) {
        return '<div id="awesome-app">' . $content . '</div>';
    }

    /**
     * Render Awesome Table
     *
     * @return string
     */
    public function awesome_table() {
        return $this->wrap_with_app(
            '<awesome-table></awesome-table>'
        );
    }
}