<?php

/**
 * Class AwesomeMotiveCore
 */
class AwesomeMotiveCore
{
    /**
     * Data api Url
     *
     * @var string $DATA_URL Data api Url
     */
    private static $DATA_URL = 'https://miusage.com/v1/challenge/1/';

    /**
     * Get data
     *
     * Checks AwesomeCache for existing data.
     *
     * If there is cached data, return that.
     * If there is NO cached data, call API, cache result and return it.
     *
     * @return mixed data
     */
    public static function get_data() {
        $cached = AwesomeCache::get_data();

        if ( $cached ) {
            return $cached;
        }

        $request = new AwesomeRequest(self::$DATA_URL);
        $data = json_decode($request->execute());

        //  Cache data
        AwesomeCache::save_data($data);

        return $data;
    }
}