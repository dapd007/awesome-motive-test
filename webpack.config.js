const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
    entry: {
        public: './public/js/src/index.js'
    },
    output: {
        filename: './[name]/js/dist/awesome-motive-[name].js',
        path: __dirname
    },
    resolve: {
        alias: {
            "@": path.resolve(__dirname, '/'),
            'vue$': 'vue/dist/vue.esm.js'
        },
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};