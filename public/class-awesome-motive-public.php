<?php

/**
 * Class AwesomeMotivePublic
 */
class AwesomeMotivePublic
{
    /**
     * Enqueue styles
     */
    public function enqueue_styles() {
        wp_enqueue_style( 'awesome_public_styles', 'https://cdn.jsdelivr.net/npm/@fanxie/leaf-css/css/leaf.min.css' );
    }

    /**
     * Enqueue and localize scripts
     */
    public function enqueue_scripts() {
        wp_enqueue_script('awesome_public_scripts', plugin_dir_url( __FILE__ ) . 'js/dist/awesome-motive-public.js', [], false, true);
        wp_localize_script( 'awesome_public_scripts', 'ajax',
            array( 'url' => admin_url( 'admin-ajax.php' ) ) );
    }
}