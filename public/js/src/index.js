import Vue from 'vue';

//  Take the ajax data object and make it available in components
Vue.prototype.$ajax = ajax;

//  Load components dynamically
const files = require.context('./components/', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//  Init Vue app
new Vue({
    el: '#awesome-app'
});