<?php

/**
 * Class RefreshCache_Command
 *
 * Clears cache and prints result
 */
class ClearCache_Command
{
    public function __invoke()
    {
        if ( AwesomeCache::clear_data() ) {
            WP_CLI::success(__('Data cache cleared', 'awesome-motive'));
        } else {
            WP_CLI::warning(__('Could not clear cache, maybe there was none?', 'awesome-motive'));
        }
    }
}