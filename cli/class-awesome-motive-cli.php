<?php

/**
 * Class AwesomeMotiveCLI
 *
 * Registers plugin CLI commands
 */
class AwesomeMotiveCLI
{
    /**
     * Load commands classes
     */
    private static function load_commands() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'cli/class-clear-cache-command.php';
    }

    /**
     * Register commands
     */
    public static function init() {
        self::load_commands();
        WP_CLI::add_command( 'awesome refresh', 'ClearCache_Command' );
    }
}