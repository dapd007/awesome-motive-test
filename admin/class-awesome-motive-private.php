<?php


class AwesomeMotivePrivate
{
    /**
     * Create simple admin menu
     */
    public function create_admin_menu() {
        add_menu_page(
            __( 'Awesome Table', 'awesome-motive' ),
            __( 'Awesome Table', 'awesome-motive' ),
            'manage_options',
            'awesome-table',
            [$this, 'render_admin_page'],
            'dashicons-editor-table',
            80
        );
    }

    /**
     * Render admin page
     * Gets data from Core and renders a table.
     */
    public function render_admin_page() {
        $data = AwesomeMotiveCore::get_data();
        $table_data = $data->data;
        ?>
        <div class="wrap">
            <h1 class="wp-heading-inline"><?php echo esc_html(__('Awesome Table Data', 'awesome-motive')) ?></h1>
            <h2><?php echo esc_html($data->title) ?></h2>
            <div class="tablenav top">
                <form action="/wp-admin/admin-post.php" method="post">
                    <input type="hidden" name="action" value="clear_awesome_cache">
                    <button type="submit" class="button button-primary"><?php echo esc_html(__('Clear cache and refresh', 'awesome-motive')) ?></button>
                </form>
            </div>
            <table class="wp-list-table widefat">
                <thead>
                <tr>
                    <?php foreach ($table_data->headers as $header): ?>
                    <th><?php echo esc_html($header); ?></th>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($table_data->rows as $row_info): ?>
                <tr>
                    <?php foreach ($row_info as $key => $value): ?>
                    <?php $this->render_row_info($key, $value) ?>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                    <?php foreach ($table_data->headers as $header): ?>
                        <th><?php echo esc_html($header); ?></th>
                    <?php endforeach; ?>
                </tr>
                </tfoot>
            </table>
        </div>
        <?php
    }

    /**
     * Render individual table rows
     *
     * @param $key
     * @param $value
     */
    private function render_row_info($key, $value) {
        if ($key === 'date'): ?>
        <td><?php echo date('m/d/Y', $value); ?></td>
        <?php else: ?>
        <td><?php echo esc_html($value) ?></td>
        <?php endif;
    }

    /**
     * Handle Clear Cache post action
     *
     * Clears cache and redirects back to table view.
     */
    public function action_clear_awesome_cache() {
        AwesomeCache::clear_data();
        AwesomeNotices::add_flash_notice(__('Cache cleared', 'awesome-motive'));
        wp_redirect( $_SERVER["HTTP_REFERER"], 302 );
        exit;
    }
}