<?php
/**
 * Plugin Name: Awesome Motive Test
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * The core plugin class that is used to define admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-awesome-motive.php';

/**
 * Begins execution of the plugin.
 */
function run_awesome_motive() {
    new AwesomeMotive();
}
run_awesome_motive();
